FROM golang
MAINTAINER bknox@digitalocean.com
COPY install.sh /
COPY debdir /debdir
COPY presentation /presentation
RUN chmod +x /install.sh
RUN /install.sh 
ENTRYPOINT ["/presentation/present.sh"]
