package main

import (
	"fmt"

	"gopkg.in/zeromq/goczmq.v4"
)

func main() {
	beacon := goczmq.NewBeacon()
	address, err := beacon.Configure(9999)
	if err != nil {
		panic(err)
	}
	defer beacon.Destroy()

	fmt.Printf("beacon on %s", address)

	beacon.Publish("HELLO", 100)
}
