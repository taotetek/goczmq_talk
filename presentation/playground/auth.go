package main

import "gopkg.in/zeromq/goczmq.v4"

func main() {
	auth := goczmq.NewAuth()
	auth.Curve(goczmq.CurveAllowAny)
	defer auth.Destroy()
}
