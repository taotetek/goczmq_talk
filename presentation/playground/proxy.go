package main

import "gopkg.in/zeromq/goczmq.v4"

func main() {
	proxy := goczmq.NewProxy()
	defer proxy.Destroy()

	proxy.SetFrontend(goczmq.Pull, "tcp://127.0.0.1:31337")
	proxy.SetBackend(goczmq.Push, "tcp://127.0.0.1:31338")
	proxy.SetCapture("tcp://127.0.0.1:31339")

	proxy.Pause()
	proxy.Resume()
}
