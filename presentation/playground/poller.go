package main

import (
	"fmt"

	"gopkg.in/zeromq/goczmq.v4"
)

func main() {
	push, _ := goczmq.NewPush("inproc://poller")
	defer push.Destroy()

	pull, _ := goczmq.NewPull("inproc://poller")
	defer pull.Destroy()

	poller, _ := goczmq.NewPoller(pull)
	poller.Add(pull)

	push.SendFrame([]byte("hello!"), goczmq.FlagNone)

	s := poller.Wait(-1)
	frame, _, _ := s.RecvFrame()
	fmt.Printf("%s\n", frame)
}
