package main

import (
	"fmt"

	"gopkg.in/zeromq/goczmq.v4"
)

func main() {
	pull, _ := goczmq.NewPull("inproc://readwriter")
	pullReader, _ := goczmq.NewReadWriter(pull)
	defer pullReader.Destroy()

	push, _ := goczmq.NewPush("inproc://readwriter")
	pushWriter, _ := goczmq.NewReadWriter(push)
	defer push.Destroy()

	pushWriter.Write([]byte("hello!"))

	buf := make([]byte, 6)
	n, _ := pull.Read(buf)
	fmt.Printf("%s\n", string(buf[:n]))
}
