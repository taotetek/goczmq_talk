package main

import (
	"fmt"

	"gopkg.in/zeromq/goczmq.v4"
)

func main() {
	pull := goczmq.NewPullChanneler("inproc://channeler")
	defer pull.Destroy()

	push := goczmq.NewPushChanneler("inproc://channeler")
	defer push.Destroy()

	push.SendChan <- [][]byte{[]byte("Hello")}

	msg := <-pull.RecvChan
	fmt.Printf("%s\n", msg[0])
}
