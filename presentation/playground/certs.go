package main

import (
	"fmt"

	"gopkg.in/zeromq/goczmq.v4"
)

func main() {
	cert := goczmq.NewCert()
	defer cert.Destroy()
	cert.SetMeta("email", "bknox@digitalocean.com")
	cert.SetMeta("name", "Brian Knox")
	cert.SetMeta("organization", "DigitalOcean")
	fmt.Printf("%s\n", cert.PublicText())
}
