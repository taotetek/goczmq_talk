package main

import (
	"fmt"

	"gopkg.in/zeromq/goczmq.v4"
)

func main() {
	push, _ := goczmq.NewPush("inproc://hello")
	defer push.Destroy()

	pull, _ := goczmq.NewPull("inproc://hello")
	defer pull.Destroy()

	push.SendFrame([]byte("hello!"), goczmq.FlagNone)
	frame, _, _ := pull.RecvFrame()

	fmt.Printf("%s\n", frame)
}
